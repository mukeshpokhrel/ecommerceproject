from django.contrib import admin
from .models import *

admin.site.register(
    [EcommerceAdmin, Customer, ProductCategory, Product, Cart, CartProduct,Order])
