from django.db import models
from django.contrib.auth.models import User


class EcommerceAdmin(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    mobile = models.CharField(max_length=30, null=True, blank=True)

    def __str__(self):
        return self.user.username


class Customer(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    mobile = models.CharField(max_length=30, null=True, blank=True)
    email = models.EmailField()
    image = models.ImageField(upload_to="customers")
    address = models.CharField(max_length=200)

    def __str__(self):
        return self.user.username


class ProductCategory(models.Model):
    title = models.CharField(max_length=200)
    slug = models.SlugField(null=True, blank=True, unique=True)
    root = models.ForeignKey(
        'self', on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return self.title


class Product(models.Model):
    title = models.CharField(max_length=200)
    category = models.ForeignKey(ProductCategory, on_delete=models.CASCADE)
    product_id = models.CharField(max_length=100, unique=True)
    image = models.ImageField(upload_to="products")
    description = models.TextField()
    max_retail_Price = models.DecimalField(max_digits=10, decimal_places=2)
    selling_Price = models.DecimalField(max_digits=10, decimal_places=2)
    # stock=models.PositiveIntegerField()

    def __str__(self):
        return self.title


class Cart(models.Model):
    customer = models.ForeignKey(
        Customer, on_delete=models.SET_NULL, null=True, blank=True)
    total = models.DecimalField(max_digits=10, decimal_places=2)

    def __str__(self):
        return "Cart ID:" + str(self.id)


class CartProduct(models.Model):
    cart = models.ForeignKey(
        Cart, on_delete=models.CASCADE, null=True, blank=True)
    product = models.OneToOneField(Product, on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField(default=1)
    rate = models.DecimalField(max_digits=10, decimal_places=2)
    subtotal = models.DecimalField(max_digits=15, decimal_places=2)

    def __str__(self):
        return self.product.title


class Order(models.Model):
    cart = models.OneToOneField(Cart, on_delete=models.CASCADE)
    subtotal = models.DecimalField(max_digits=15, decimal_places=2)
    discount = models.DecimalField(max_digits=10, decimal_places=2)
    total = models.DecimalField(max_digits=15, decimal_places=2)
    order_date = models.DateTimeField(auto_now_add=True)
    city = models.CharField(max_length=200)
    street_address = models.CharField(max_length=200)
    mobile = models.CharField(max_length=15)
    delivery_date = models.DateField()

    def __str__(self):
        return self.cart.customer.name + "(Order ID: " + str(self.id) + ")"
